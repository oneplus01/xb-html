

odoo.define('recently_viewed_records.Storage', function (require) {
    "use strict";
    var FormRenderer = require('web.FormRenderer');
    var local_storage = require('web.local_storage');

    var RECENTLY_RECORDS_KEY = 'recently_viewed_records'

    var get_record_key = function get_record_key(record) {
        return _.str.sprintf('%s,%s', record.id, record.model);
    };

    var recent_manager = {
        get: function() {
            var records = local_storage.getItem(RECENTLY_RECORDS_KEY);
            return records ? JSON.parse(records) : [];
        },
        set: function(records) {
            local_storage.setItem(RECENTLY_RECORDS_KEY, JSON.stringify(records));
        }
    }

    FormRenderer.include({
        _updateView: function(e) {
            this._super.apply(this, arguments);
            var records = recent_manager.get();
            var data = {'url':this.$el.context.baseURI,'id':this.state.data.id,'model':this.state.model}
            if(this.state.context.params) {
                data['act'] = this.state.context.params.action
            }
            var record_key = get_record_key(data);
            data['name'] = this.state.data.display_name || record_key;
            var index = _.findIndex(records, function(record) {
                return get_record_key(record) === record_key;
            })
            if (index === -1) {
                records.unshift(data);
                recent_manager.set(records.slice(0, 10))
            }
        },
    });

    return recent_manager;

})

odoo.define('recently_viewed_records.Widget', function (require) {
    "use strict";
    
    var core = require('web.core');
    var SystrayMenu = require('web.SystrayMenu');
    var Widget = require('web.Widget');
    var recent_manager = require('recently_viewed_records.Storage');
    var local_storage = require('web.local_storage');
    var QWeb = core.qweb;

    
    var RecentRecord = Widget.extend({
        template:'recently_viewed_records.RecentRecord',
        events: {
            'show.bs.dropdown': '_onRecordMenuClick',
            "click .recently_records_dropdown a": "_onRecordFilterClick",
        },
        init: function() {
            this._super.apply(this, arguments);
            this.state = {
                records: []
            }
        },
        start: function () {
            this.$records_dropdown = this.$('.recently_records_dropdown');
            this._updateRecordPreview();
            return this._super();
        },
    
        /* Private */
        _getRecordData: function(){
            return recent_manager.get();
        },
        _isOpen: function () {
            return this.$el.hasClass('show');
        },
        _updateRecordPreview: function () {
            var records = this._getRecordData();
            this.setRecords(records);
            this.render_records();
        },
        setRecords: function(records) {
            this.state.records = records
        },
        render_records: function() {
            this.$records_dropdown.html(QWeb.render('recently_viewed_records.RecentRecordPreview', {
                records : this.state.records
            }));
        },

        /* Handlers */

        _onRecordFilterClick: function (event) {
            event.preventDefault()
            var data = _.extend({}, $(event.currentTarget).data(), $(event.target).data());

            this.do_action({
                type: 'ir.actions.act_window',
                name: data.model_name,
                res_model:  data.res_model,
                res_id:  data.res_id,
                views: [[false, 'form']],
                search_view_id: [false],
                target:'current',
            });
        },

        _onRecordMenuClick: function (event) {
            if (!this._isOpen()) {
                this._updateRecordPreview();
            }
        },
    });
    
    SystrayMenu.Items.push(RecentRecord);

    return RecentRecord
});